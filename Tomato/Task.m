//
//  Task.m
//  Tomato
//
//  Created by Lin Teng on 4/12/13.
//  Copyright (c) 2013 Teng Lin. All rights reserved.
//

#import "Task.h"
#import "Pomodoro.h"


@implementation Task

@dynamic taskName;
@dynamic taskPriority;
@dynamic pomodoro;

@end
