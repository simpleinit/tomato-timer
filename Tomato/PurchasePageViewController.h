//
//  PurchasePageViewController.h
//  Tomato
//
//  Created by Lin Teng on 14-7-18.
//  Copyright (c) 2014年 Teng Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchasePageViewController : UIViewController
{
    int pageNumber;
}
@property (weak, nonatomic) IBOutlet UIView *ImageBackView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

- (id)initWithPageNumber:(int)page;

@end
