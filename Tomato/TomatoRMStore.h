//
//  TomatoRMStore.h
//  Tomato
//
//  Created by Teng Lin on 14-7-8.
//  Copyright (c) 2014年 Teng Lin. All rights reserved.
//

#import <Foundation/Foundation.h>


//for tomato timer Unlock advance features
extern NSString *const UnlockAllProductIdentifier;

@interface TomatoRMStore : NSObject

@end
