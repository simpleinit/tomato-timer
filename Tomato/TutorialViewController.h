//
//  TutorialViewController.h
//  Tomato
//
//  Created by Lin Teng on 6/4/13.
//  Copyright (c) 2013 Teng Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <UIScrollViewDelegate>

@end
