//
//  PurchaseViewController.m
//  Tomato
//
//  Created by Lin Teng on 14-7-18.
//  Copyright (c) 2014年 Teng Lin. All rights reserved.
//
#import <StoreKit/StoreKit.h>
#import "PurchaseViewController.h"
#import "PurchasePageViewController.h"
#import "RMStore.h"
#import "RMStoreKeychainPersistence.h"
#import "TomatoRMStore.h"
#import "Reachability.h"
#import "DataManager.h"

static NSUInteger kNumberOfPurchasePages = 4;

@interface PurchaseViewController () {
    BOOL _pageControlUsed;
    
    NSArray *_products;
    RMStoreKeychainPersistence *_persistence;
    BOOL _connnectionToStoreIsRunning;
    BOOL _productsInfoObtained;
    BOOL _productPurchased;
}

// if user has unlocked successfully, show this view!
@property (strong, nonatomic) UITextView *unlockedView;


@property (nonatomic, weak) IBOutlet UIImageView *backImageView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIButton *purchaseButton;
@property (nonatomic, weak) IBOutlet UIButton *restoreButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic, strong) NSMutableArray *viewControllers;

- (IBAction)changePage:(id)sender;
- (IBAction)purchaseTapped:(id)sender;
- (IBAction)restoreTapped:(id)sender;
- (IBAction)cancelTapped:(id)sender;

@end

@implementation PurchaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (IS_WIDESCREEN) {
        self.backImageView.image = [UIImage imageNamed:@"tutorial_background_568.png"];
    }
    else {
        self.backImageView.image = [UIImage imageNamed:@"tutorial_background.png"];
    }
    
    _products = @[UnlockAllProductIdentifier];
    [self checkPurchasesInfoFirstTime];    
}

- (void)showNeedUnlockScreen
{
    // Do any additional setup after loading the view from its nib.
    // view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPurchasePages; i++)
    {
		[controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    
    self.pageControl.numberOfPages = kNumberOfPurchasePages;
    self.pageControl.currentPage = 0;
    
    self.purchaseButton.backgroundColor = [UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:0.1];
    self.restoreButton.backgroundColor = [UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:0.1];
    self.purchaseButton.clipsToBounds = YES;
    self.purchaseButton.layer.cornerRadius = 5.0f;
    self.restoreButton.clipsToBounds = YES;
    self.restoreButton.layer.cornerRadius = 5.0f;
    
    //[self.getStartedButton setBackgroundImage:[UIImage imageNamed:@"get_started_button.png"] forState:UIControlStateNormal];
    //[self.getStartedButton setBackgroundImage:[UIImage imageNamed:@"get_started_button_highlight.png"] forState:UIControlStateHighlighted | UIControlStateSelected];
    
    //set price as en_US
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    if (usLocale) {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
        numberFormatter.locale = usLocale;
        NSString *formattedString = [numberFormatter stringFromNumber:[NSDecimalNumber numberWithFloat:0.99]];
        [self.purchaseButton setTitle:[NSString stringWithFormat:@"Purchase All %@",formattedString] forState:UIControlStateNormal];
    }
    self.unlockedView.hidden = YES;
}

- (void)showHasUnlockedScreen
{
    if (!self.unlockedView) {
        self.unlockedView = [[UITextView alloc] initWithFrame:CGRectMake(10, 100, 300, 100)];
        self.unlockedView.backgroundColor = [UIColor clearColor];
        self.unlockedView.text = @"You have unlocked all features successfully. Enjoy!";
        self.unlockedView.textColor = [UIColor colorWithRed:1.0 green:204.0/255.0 blue:102.0/255.0 alpha:1.0];
        self.unlockedView.textAlignment = NSTextAlignmentCenter;
        self.unlockedView.font = [UIFont systemFontOfSize:17.0];
        [self.view addSubview:self.unlockedView];
    }
    self.unlockedView.hidden = NO;
    self.scrollView.hidden = YES;
    self.purchaseButton.hidden = YES;
    self.restoreButton.hidden = YES;
    self.pageControl.hidden = YES;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_productPurchased) {
        return;
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * kNumberOfPurchasePages, self.scrollView.frame.size.height);
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    [self loadScrollViewWithPage:2];
    [self loadScrollViewWithPage:3];
    
    
    
    /*
    _getStartedPosition  = self.getStartedButton.layer.position;
    self.getStartedButton.layer.position = CGPointMake(_getStartedPosition.x, _getStartedPosition.y + 60);
    
    self.pageControl.alpha = 0.0;
    [UIView animateWithDuration:0.7
                     animations:^{
                         self.pageControl.alpha = 1.0;
                     }
                     completion:^(BOOL  completed){
                     }];
    
#if DEBUG_MAKE_DEFAULT_PNG
    UILabel *bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, self.view.frame.size.height-55, 260, 50)];
    bottomLabel.textAlignment = NSTextAlignmentCenter;
    bottomLabel.textColor = [UIColor whiteColor];
    bottomLabel.font = [UIFont systemFontOfSize:16];
    bottomLabel.backgroundColor = [UIColor clearColor];
    bottomLabel.text = @"A Time Management Method";
    [self.view addSubview:bottomLabel];
#endif
    */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    if (page >= kNumberOfPurchasePages)
        return;
    
    // replace the placeholder if necessary
    PurchasePageViewController *controller = [self.viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null])
    {
        controller = [[PurchasePageViewController alloc] initWithPageNumber:page];
        [self.viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil)
    {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (_pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    
    // A possible optimization would be to unload the views+controllers which are no longer visible
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    _pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControlUsed = NO;
}

- (IBAction)changePage:(id)sender
{
    int page = self.pageControl.currentPage;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    _pageControlUsed = YES;
   
}

// ios7 and later
// fix we must have status bar show, because the network indicator is neccessary to users!
/*
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
*/

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - iap

- (void)checkPurchasesInfoFirstTime
{
    _persistence = [RMStore defaultStore].transactionPersistor;
    
#if DEBUG_IAP
    _productPurchased = NO;
#else
    _productPurchased = [_persistence isPurchasedProductOfIdentifier:UnlockAllProductIdentifier];
#endif
    
    if (_productPurchased) {
        [self showHasUnlockedScreen];
    } else {
        [self showNeedUnlockScreen];
        [self requestProductsListThenBuy:NO popAlert:NO];
    }
}

- (void)requestProductsListThenBuy:(BOOL)buyIfSuccess popAlert:(BOOL)showAlert
{
    if (![Reachability reachableForInternetConnection]) {
        if (showAlert) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network is unavailable" message:@"Please check your network settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        return;
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    _connnectionToStoreIsRunning = YES;
    [[RMStore defaultStore] requestProducts:[NSSet setWithArray:_products] success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _connnectionToStoreIsRunning = NO;
        [self checkIfResponsedProductAndPriceAsExpectWithAlert:showAlert];
        if (_productsInfoObtained && buyIfSuccess) {
            [self buyProduct];
        }
    } failure:^(NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _connnectionToStoreIsRunning = NO;
        if (showAlert) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Products Request Failed", @"")
                                                                message:error.localizedDescription
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    }];
}

- (void)checkIfResponsedProductAndPriceAsExpectWithAlert:(BOOL)showAlert
{
    NSLog(@"checkIfResponsedProductAndPriceAsExpect");
    SKProduct *product = [[RMStore defaultStore] productForIdentifier:UnlockAllProductIdentifier];
    if (product && product.price) {
        _productsInfoObtained = YES;
        NSLog(@"Can Buy!");
        [self.purchaseButton setTitle:[NSString stringWithFormat:@"Purchase All %@",[RMStore localizedPriceOfProduct:product]] forState:UIControlStateNormal];
    }
    else {
        _productsInfoObtained = NO;
        NSLog(@"Can not Buy!, show Alert!");
        if (showAlert) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Products Request Failed", @"")
                                                                message:@"Please check your device settings, and make sure it's not jailbroken."
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        
    }
}

- (IBAction)cancelTapped:(id)sender
{
#if DEBUG_IAP
    [_persistence removeTransactions];
#endif
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)purchaseTapped:(id)sender
{
    if(_connnectionToStoreIsRunning) {
        return;
    }
    else if (_productsInfoObtained) {
        [self buyProduct];
    }
    else {
        [self requestProductsListThenBuy:YES popAlert:YES];
    }
}

- (IBAction)restoreTapped:(id)sender
{
    /* for test
    [[DataManager sharedInstance] deleteObjectsBeforeTodayInDB];
    return;
     */
    
    ClawVerbose(@"BEGIN");
    //[[RageIAPHelper sharedInstance] restoreCompletedTransactions];
    if(_connnectionToStoreIsRunning) {
        return;
    }
    else {
        [self restoreAction];
    }
    ClawVerbose(@"END");
}

- (void)buyProduct
{
    if (![RMStore canMakePayments]) {
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"")
                                                           message:@"You are not allowed to make payments, Please check your settings."
                                                          delegate:nil
                                                 cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                 otherButtonTitles:nil];
        [alerView show];
        return;
    }
    
    if (![Reachability reachableForInternetConnection]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network is unavailable" message:@"Please check your network settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    _connnectionToStoreIsRunning = YES;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [[RMStore defaultStore] addPayment:UnlockAllProductIdentifier success:^(SKPaymentTransaction *transaction) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _connnectionToStoreIsRunning = NO;
        [self checkIfBuyOrRestoreSuccessAsExpect];
    } failure:^(SKPaymentTransaction *transaction, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _connnectionToStoreIsRunning = NO;
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Payment Transaction Failed", @"")
                                                           message:error.localizedDescription
                                                          delegate:nil
                                                 cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                 otherButtonTitles:nil];
        [alerView show];
    }];
    
    
}

- (void)restoreAction
{
    if (![Reachability reachableForInternetConnection]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network is unavailable" message:@"Please check your network settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    _connnectionToStoreIsRunning = YES;
    [[RMStore defaultStore] restoreTransactionsOnSuccess:^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _connnectionToStoreIsRunning = NO;
        [self checkIfBuyOrRestoreSuccessAsExpect];
    } failure:^(NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _connnectionToStoreIsRunning = NO;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Restore Transactions Failed", @"")
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
}

- (void)checkIfBuyOrRestoreSuccessAsExpect
{
    ClawVerbose(@"checkIfBuyOrRestoreSuccessAsExpect");
    _productPurchased = [_persistence isPurchasedProductOfIdentifier:UnlockAllProductIdentifier];
    if (_productPurchased) {
        [self showHasUnlockedScreen];        
        [self.cancelButton setTitle:@"Done" forState:UIControlStateNormal];
        [[DataManager sharedInstance] deleteObjectsBeforeTodayInDB];
    }
    else {
        // alert something wrong!
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Transactions Failed", @"")
                                                            message:@"Please check your device settings, and make sure it's not jailbroken."
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}


@end
