//
//  main.m
//  Tomato
//
//  Created by Teng Lin on 12-12-29.
//  Copyright (c) 2012年 Teng Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
