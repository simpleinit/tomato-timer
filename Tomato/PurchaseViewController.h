//
//  PurchaseViewController.h
//  Tomato
//
//  Created by Lin Teng on 14-7-18.
//  Copyright (c) 2014年 Teng Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchaseViewController : UIViewController <UIScrollViewDelegate>

@end
