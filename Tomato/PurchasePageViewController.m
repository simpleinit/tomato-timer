//
//  PurchasePageViewController.m
//  Tomato
//
//  Created by Lin Teng on 14-7-18.
//  Copyright (c) 2014年 Teng Lin. All rights reserved.
//

#import "PurchasePageViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PurchasePageViewController ()

@end

@implementation PurchasePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor clearColor];
    self.ImageBackView.backgroundColor = [UIColor whiteColor];
    self.textView.backgroundColor = [UIColor clearColor];
#warning perform loadContentForPageNumber:pageNumber in viewWillAppear will cause a very weird error, seems scrollview will auto scroll!! so we should do it here.
    [self loadContentForPageNumber:pageNumber];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self customSubViewsLayout];
#warning loadContentForPageNumber:pageNumber here will cause a very weird error, seems scrollview will auto scroll!!
    //[self loadContentForPageNumber:pageNumber];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithPageNumber:(int)page
{
    if (self = [super initWithNibName:@"PurchasePageViewController" bundle:nil])
    {
        pageNumber = page;
        // loading image here do not works, since view not loaded! so should do it later
        // [self loadContentForPageNumber:page];
    }
    return self;
}

#pragma mark - layout

- (void)customSubViewsLayout
{
    if (IS_WIDESCREEN) {
        // code for 4-inch screen
        if (IS_IOS7_OR_HIGHER) {
            self.ImageBackView.frame = CGRectMake(60, 80, 200, 196);
            self.imageView.frame = CGRectMake(65, 85, 190, 190);
            self.textView.frame = CGRectMake(20, 300, 280, 150);
        }
        else {
            self.ImageBackView.frame = CGRectMake(60, 60, 200, 196);
            self.imageView.frame = CGRectMake(65, 65, 190, 190);
            self.textView.frame = CGRectMake(20, 280, 280, 150);
        }
    }
    else {
        // code for 3.5-inch screen
        if (IS_IOS7_OR_HIGHER) {
            self.ImageBackView.frame = CGRectMake(75, 65, 170, 167);
            self.imageView.frame = CGRectMake(80, 70, 160, 160);
            //self.ImageBackView.frame = CGRectMake(25, 0, 270, 270);
            //self.imageView.frame = CGRectMake(30, 5, 260, 260);
            self.textView.frame = CGRectMake(20, 235, 280, 135);
        }
        else {
            self.ImageBackView.frame = CGRectMake(75, 45, 170, 167);
            self.imageView.frame = CGRectMake(80, 50, 160, 160);
            //self.ImageBackView.frame = CGRectMake(25, 0, 270, 270);
            //self.imageView.frame = CGRectMake(30, 5, 260, 260);
            self.textView.frame = CGRectMake(20, 215, 280, 130);
        }
        
    }
}

- (void)loadContentForPageNumber:(NSInteger)page
{
    
    self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"PurchaseAd%d.png", page]];
    
    switch (page) {
        case 0:
            self.textView.text = @"1. View the details of today's pomodoros, you can also delete some of them if you want."; //查看你今天完成的所有番茄时间的详细情况。如果需要，你还可以选择性删除其中一些。";
            break;
        case 1:
            self.textView.text = @"2. Show today's pomodoros in a 24-hour clock, let you know today's productivity in a vivid way.";//以24小时钟表的方式展现你今天完成的番茄种，让你能了解这些番茄中在一天中是如何分布的。这能帮助你更直观地了解今天的工作效率。";
            break;
        case 2:
            self.textView.text = @"3. Record and view pomodoros history of recent days, you can clear history when you need a new start.";//查看你最近这些天完成的番茄时间的历史记录，这会有助你了解工作效率的变化情况。你还可以在适当的时候清除历史记录，并且重新开始记录历史。";
            break;
        case 3:
            self.textView.text = @"4. View pomodoros history in a chart, help you know the productivity of recent days as a whole.";//4: 以图表的方式查看过去的历史记录，更直观，这也许能帮助你找到提高工作效率的灵感。";
            break;
        case 4:
            self.textView.text = @"你的支持会鼓励我们继续开发，谢谢！";
            break;
        default:
            break;
    }
}


@end
