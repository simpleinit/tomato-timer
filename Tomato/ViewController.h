//
//  ViewController.h
//  Tomato
//
//  Created by Teng Lin on 12-12-29.
//  Copyright (c) 2012年 Teng Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IASKAppSettingsViewController.h"
#import "TomatoManagerDelegate.h"

@interface ViewController : UIViewController <UIActionSheetDelegate, IASKSettingsDelegate, TomatoManagerDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
