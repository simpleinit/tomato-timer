//
//  SharingServiceType.h
//  Tomato
//
//  Created by Lin Teng on 14-7-14.
//  Copyright (c) 2014年 Teng Lin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    TellFriendsTwitter,
    TellFriendsFacebook,
    TellFriendsSinaWeibo,
    TellFriendsEmail,
    TellFriendsMessage,
    TellFriendsWechat,
} TellFriendsType;

@interface SharingServiceType : NSObject

@property TellFriendsType type;
@property (strong) NSString * name;
@property (strong) NSString * imageName;

@end
