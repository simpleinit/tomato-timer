//
//  Pomodoro.m
//  Tomato
//
//  Created by Lin Teng on 4/12/13.
//  Copyright (c) 2013 Teng Lin. All rights reserved.
//

#import "Pomodoro.h"
#import "Day.h"
#import "Task.h"


@implementation Pomodoro

@dynamic bDuration;
@dynamic bEndDate;
@dynamic bInterrupted;
@dynamic bStartDate;
@dynamic pDuration;
@dynamic pEndDate;
@dynamic pInterrupted;
@dynamic pStartDate;
@dynamic day;
@dynamic task;

@synthesize sectionDay;
@synthesize pDurationString;
@synthesize bDurationString;

- (NSString *)sectionDay
{
    //[self willAccessValueForKey:@"sectionDay"];
    //NSString *temp = sectionDay;
    //[self didAccessValueForKey:@"sectionDay"];
    NSString *temp = nil;
    
    if(!temp)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd EEEE"]; // format your section titles however you want
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        temp = [dateFormatter stringFromDate:self.pEndDate];
        sectionDay = temp;
    }
    
    return temp;
}

- (NSString*)pDurationString
{
    // [self willAccessValueForKey:@"pDurationString"];
    // NSString *temp = pDurationString;
    // [self didAccessValueForKey:@"pDurationString"];    
     NSString *temp = nil;
    
    if(!temp)
    {
        if (self.pDuration.intValue <= 90) {
            temp = [NSString stringWithFormat:@"%d sec", self.pDuration.intValue];
        }
        else if (90 < self.pDuration.intValue ) {
            temp = [NSString stringWithFormat:@"%d min", (int)ceil(self.pDuration.intValue/60)  ];
        }
        
        pDurationString = temp;
    }    
    return temp;

}

- (NSString*)bDurationString
{
    //[self willAccessValueForKey:@"bDurationString"];
    //NSString *temp = bDurationString;
    //[self didAccessValueForKey:@"bDurationString"];
    NSString *temp = nil;
    
    if(!temp)
    {
        if (self.bDuration.intValue <= 90) {
            temp = [NSString stringWithFormat:@"%d sec", self.bDuration.intValue];
        }
        else if (90 < self.bDuration.intValue ) {
            temp = [NSString stringWithFormat:@"%d min", (int)ceil(self.bDuration.intValue/60)  ];
        }
        
        bDurationString = temp;
    }
    
    return temp;
    
}


@end
