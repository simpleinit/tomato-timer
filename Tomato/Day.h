//
//  Day.h
//  Tomato
//
//  Created by Lin Teng on 4/12/13.
//  Copyright (c) 2013 Teng Lin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Pomodoro;

@interface Day : NSManagedObject

@property (nonatomic, retain) NSDate * beginOfDay;
@property (nonatomic, retain) NSNumber * pomodoroCount;
@property (nonatomic, retain) NSNumber * pomodoroSeconds;
@property (nonatomic, retain) NSSet *pomodoro;

@property (nonatomic, retain) NSString *sectionMonth;
@end

@interface Day (CoreDataGeneratedAccessors)

- (void)addPomodoroObject:(Pomodoro *)value;
- (void)removePomodoroObject:(Pomodoro *)value;
- (void)addPomodoro:(NSSet *)values;
- (void)removePomodoro:(NSSet *)values;

@end
