//
//  Vars.h
//  Tomato
//
//  Created by Lin Teng on 4/12/13.
//  Copyright (c) 2013 Teng Lin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Vars : NSManagedObject

@property (nonatomic, retain) NSNumber * biggestPSecondsOfAllDay;
@property (nonatomic, retain) NSDate * firstSavedDayBegin;

@end
