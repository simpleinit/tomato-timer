//
//  TutorialContentViewController.h
//  Tomato
//
//  Created by Lin Teng on 6/4/13.
//  Copyright (c) 2013 Teng Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialPageViewController : UIViewController
{
    int pageNumber;
}

@property (weak, nonatomic) IBOutlet UIView *ImageBackView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

- (id)initWithPageNumber:(int)page;



@end
