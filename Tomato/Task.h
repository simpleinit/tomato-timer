//
//  Task.h
//  Tomato
//
//  Created by Lin Teng on 4/12/13.
//  Copyright (c) 2013 Teng Lin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Pomodoro;

@interface Task : NSManagedObject

@property (nonatomic, retain) NSString * taskName;
@property (nonatomic, retain) NSNumber * taskPriority;
@property (nonatomic, retain) NSSet *pomodoro;
@end

@interface Task (CoreDataGeneratedAccessors)

- (void)addPomodoroObject:(Pomodoro *)value;
- (void)removePomodoroObject:(Pomodoro *)value;
- (void)addPomodoro:(NSSet *)values;
- (void)removePomodoro:(NSSet *)values;

@end
