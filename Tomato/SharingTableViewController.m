//
//  SharingTableViewController.m
//  Tomato
//
//  Created by Lin Teng on 14-7-14.
//  Copyright (c) 2014年 Teng Lin. All rights reserved.
//

#import "SharingTableViewController.h"
#import "SharingServiceType.h"
#import "Reachability.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface SharingTableViewController () <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property NSMutableArray * arrayOfAvailableServices;
@property NSMutableArray * arrayOfUnavailableServices;

@end

@implementation SharingTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.arrayOfAvailableServices = [[NSMutableArray alloc] init];
    self.arrayOfUnavailableServices = [[NSMutableArray alloc] init];
    [self checkServicesAvailability];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.arrayOfAvailableServices count] + [self.arrayOfUnavailableServices count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"shareCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shareCellIdentifier" forIndexPath:indexPath];
    // Configure the cell...
    NSUInteger availableCount = [self.arrayOfAvailableServices count];
    NSUInteger unavailableCount = [self.arrayOfUnavailableServices count];
    
    if (indexPath.row < availableCount) {
        cell.textLabel.text = [[self.arrayOfAvailableServices objectAtIndex:indexPath.row] name];
        cell.imageView.image = [UIImage imageNamed:[[self.arrayOfAvailableServices objectAtIndex:indexPath.row] imageName]];
    }
    else if (indexPath.row < availableCount + unavailableCount){
        cell.textLabel.text = [[self.arrayOfUnavailableServices objectAtIndex:(indexPath.row - availableCount)] name];
        cell.imageView.image = [UIImage imageNamed:[[self.arrayOfUnavailableServices objectAtIndex:(indexPath.row - availableCount)] imageName]];
    }
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52;
}
*/

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    [self shareToFriends:indexPath.row];
}

#pragma mark - others
- (void)checkServicesAvailability
{
    SharingServiceType * newService;
    BOOL isAvailable = [SLComposeViewController isAvailableForServiceType: SLServiceTypeTwitter];
    if(isAvailable) {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsTwitter;
        newService.name = @"Twitter";
        newService.imageName = @"twitter.png";
        [self.arrayOfAvailableServices addObject: newService];
    }
    else {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsTwitter;
        newService.name = @"Twitter";
        newService.imageName = @"twitter.png";
        [self.arrayOfUnavailableServices addObject: newService];
    }
    
    isAvailable = [SLComposeViewController isAvailableForServiceType: SLServiceTypeFacebook];
    if(isAvailable)
    {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsFacebook;
        newService.name = @"Facebook";
        newService.imageName = @"facebook.png";
        [self.arrayOfAvailableServices addObject: newService];
    }
    else {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsFacebook;
        newService.name = @"Facebook";
        newService.imageName = @"facebook.png";
        [self.arrayOfUnavailableServices addObject: newService];
    }
    
    isAvailable = [SLComposeViewController isAvailableForServiceType: SLServiceTypeSinaWeibo];
    if(isAvailable)
    {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsSinaWeibo;
        newService.name =  @"Sina Weibo";
        newService.imageName = @"sinaWeibo.png";
        [self.arrayOfAvailableServices addObject: newService];
    }
    else {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsSinaWeibo;
        newService.name =  @"Sina Weibo";
        newService.imageName = @"sinaWeibo.png";
        [self.arrayOfUnavailableServices addObject: newService];
    }
    
    // check for Email
    isAvailable = [MFMailComposeViewController canSendMail];
    if(isAvailable)
    {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsEmail;
        newService.name =  @"Email";
        newService.imageName = @"email.png";
        [self.arrayOfAvailableServices addObject: newService];
    }
    else {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsEmail;
        newService.name =  @"Email";
        newService.imageName = @"email.png";
        [self.arrayOfUnavailableServices addObject: newService];
    }
    
    // check for Sms
    isAvailable = [MFMessageComposeViewController canSendText];
    if(isAvailable)
    {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsMessage;
        newService.name =  @"Messages";
        newService.imageName = @"message.png";
        [self.arrayOfAvailableServices addObject: newService];
    }
    else {
        newService = [[SharingServiceType alloc] init];
        newService.type = TellFriendsMessage;
        newService.name =  @"Messages";
        newService.imageName = @"message.png";
        [self.arrayOfUnavailableServices addObject: newService];
    }
}

- (void)shareToFriends:(NSUInteger)serviceIndex
{
    NSUInteger availableCount = [self.arrayOfAvailableServices count];
    NSUInteger unavailableCount = [self.arrayOfUnavailableServices count];
    
    SharingServiceType *sharingServiceType;
    if (serviceIndex < availableCount) {
        sharingServiceType = [self.arrayOfAvailableServices objectAtIndex:serviceIndex];
    }
    else if (serviceIndex < availableCount + unavailableCount) {
        sharingServiceType = [self.arrayOfUnavailableServices objectAtIndex:(serviceIndex - availableCount)];
    }
    
    if (!sharingServiceType) {
        return;
    }

    if (sharingServiceType.type != TellFriendsMessage && ![Reachability reachableForInternetConnection]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network is unavailable" message:@"Please check your network settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    switch (sharingServiceType.type) {
        case TellFriendsTwitter:
            [self shareBySocialNetwork:SLServiceTypeTwitter withName:sharingServiceType.name];
            break;
        case TellFriendsFacebook:
            [self shareBySocialNetwork:SLServiceTypeFacebook withName:sharingServiceType.name];
            break;
        case TellFriendsSinaWeibo:
            [self shareBySocialNetwork:SLServiceTypeSinaWeibo withName:sharingServiceType.name];
            break;
        case TellFriendsEmail:
            [self shareByEmail:sharingServiceType.name];
            break;
        case TellFriendsMessage:
            [self shareByMessage:sharingServiceType.name];
            break;
        default:
            break;
    }
}

// only for ios native supported: twitter, facebook, weibo
- (void)shareBySocialNetwork:(NSString*)serviceType withName:(NSString*)serviceName
{
    if([SLComposeViewController isAvailableForServiceType:serviceType]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:serviceType];
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                NSLog(@"ResultCancelled");
            }
            else {
                NSLog(@"Success");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thanks" message:@"Thank you for your support!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        [controller setInitialText:@"Tomato Timer is a nice iOS app to help manage your time. Free on App Store."];
        [controller addURL:[NSURL URLWithString:@"http://itunes.apple.com/app/id701368955"]];
        [controller addImage:[UIImage imageNamed:@"Icon.png"]];
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else{
        NSString *settingsTitle = [NSString stringWithFormat:@"No %@ Account", serviceName];
        NSString *settingsMessage = [NSString stringWithFormat:@"There is no %@ account configured. You can add or create a %@ account in the Settings of your device.",serviceName, serviceName];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:settingsTitle message:settingsMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        NSLog(@"UnAvailable");
    }
}

// Email
- (void)shareByEmail:(NSString*)serviceName
{
    if ([MFMailComposeViewController canSendMail])
        // The device can send email.
    {
        [self displayMailComposerSheet];
    }
    else
        // The device can not send email.
    {
        NSString *settingsTitle = [NSString stringWithFormat:@"No %@ Account", serviceName];
        NSString *settingsMessage = [NSString stringWithFormat:@"There is no %@ account configured. You can add a %@ account in the Settings of your device.",serviceName, serviceName];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:settingsTitle message:settingsMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

// Message
- (void)shareByMessage:(NSString*)serviceName
{
    if ([MFMessageComposeViewController canSendText])
        // The device can send email.
    {
        [self displaySMSComposerSheet];
    }
    else
        // The device can not send email.
    {
        NSString *settingsTitle = [NSString stringWithFormat:@"No %@ Account", serviceName];
        NSString *settingsMessage = [NSString stringWithFormat:@"There is no %@ account configured. You can add a %@ account in the Settings of your device.",serviceName, serviceName];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:settingsTitle message:settingsMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}


//
// -------------------------------------------------------------------------------
//	displayMailComposerSheet
//  Displays an email composition interface inside the application.
//  Populates all the Mail fields.
// -------------------------------------------------------------------------------
- (void)displayMailComposerSheet
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Tomato Timer is a nice App."];
	
	// Set up recipients
    /*
	NSArray *toRecipients = [NSArray arrayWithObject:@"first@example.com"];
	NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil];
	NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com"];
	
	[picker setToRecipients:toRecipients];
	[picker setCcRecipients:ccRecipients];
	[picker setBccRecipients:bccRecipients];
     */
	
	// Attach an image to the email
	NSString *path = [[NSBundle mainBundle] pathForResource:@"Icon" ofType:@"png"];
	NSData *myData = [NSData dataWithContentsOfFile:path];
	[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"Icon"];
	
	// Fill out the email body text
	//NSString *emailBody = @"Tomato Timer can help you manage your time. It can be found on App Store. http://www.itunes.com/";
    NSString *emailBody = @"<html><body>Tomato Timer is a nice iOS app can help you manage your time. It's free on App Store.<a href=\"http://itunes.apple.com/app/id701368955\">click here to go to App Store.</a></body></html>";

	[picker setMessageBody:emailBody isHTML:YES];
	
	[self presentViewController:picker animated:YES completion:NULL];
}

// -------------------------------------------------------------------------------
//	displayMailComposerSheet
//  Displays an SMS composition interface inside the application.
// -------------------------------------------------------------------------------
- (void)displaySMSComposerSheet
{
	MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
	picker.messageComposeDelegate = self;
	
    // You can specify one or more preconfigured recipients.  The user has
    // the option to remove or add recipients from the message composer view
    // controller.
    /* picker.recipients = @[@"Phone number here"]; */
    
    // You can specify the initial message text that will appear in the message
    // composer view controller.
    picker.body = @"Tomato Timer is a nice iOS app can help you manage your time. It's free on App Store. http://itunes.apple.com/app/id701368955";
    
	[self presentViewController:picker animated:YES completion:NULL];
}


#pragma mark - Delegate Methods

// -------------------------------------------------------------------------------
//	mailComposeController:didFinishWithResult:
//  Dismisses the email composition interface when users tap Cancel or Send.
//  Proceeds to update the message field with the result of the operation.
// -------------------------------------------------------------------------------
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	// Notifies users about errors associated with the interface
    //NSString *messageInfo;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			//self.feedbackMsg.text = @"Result: Mail sending canceled";
			break;
		case MFMailComposeResultSaved:
			//self.feedbackMsg.text = @"Result: Mail saved";
			break;
		case MFMailComposeResultSent:
			//self.feedbackMsg.text = @"Result: Mail sent";
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mail has been sent" message:@"Thanks for your support!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
			break;
		case MFMailComposeResultFailed:
			//self.feedbackMsg.text = @"Result: Mail sending failed";
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mail sending failed" message:@"Please check your Settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
			break;
		default:
			//self.feedbackMsg.text = @"Result: Mail not sent";
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mail not sent" message:@"Please check your Settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:NULL];
}

// -------------------------------------------------------------------------------
//	messageComposeViewController:didFinishWithResult:
//  Dismisses the message composition interface when users tap Cancel or Send.
//  Proceeds to update the feedback message field with the result of the
//  operation.
// -------------------------------------------------------------------------------
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MessageComposeResultCancelled:
			//self.feedbackMsg.text = @"Result: SMS sending canceled";
			break;
		case MessageComposeResultSent:
        {
			//self.feedbackMsg.text = @"Result: SMS sent";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message has been sent" message:@"Thanks for your support!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
			break;
		case MessageComposeResultFailed:
        {
			//self.feedbackMsg.text = @"Result: SMS sending failed";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message sending failed" message:@"Please check your Settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
			break;
		default:
        {
			//self.feedbackMsg.text = @"Result: SMS not sent";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message not sent" message:@"Please check your Settings and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:NULL];
}

@end
